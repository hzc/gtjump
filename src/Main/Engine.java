package Main;
import java.awt.Graphics;
import java.util.LinkedList;

public class Engine {
LinkedList<GameObject> Oblist = new LinkedList<GameObject>();
	
	public void tick(){
		for(int i=0 ; i < Oblist.size() ; i++){
			GameObject tempObject = Oblist.get(i);
			tempObject.tick();
		}
	}
	
	public void render(Graphics g){
		for(int i=0 ; i < Oblist.size() ; i++){
			GameObject tempObject = Oblist.get(i);
			tempObject.render(g);
		}
	}
	
	public void addObject(GameObject q){
		this.Oblist.add(q);
	}
	
	public void removeObject(GameObject q){
		this.Oblist.remove(q);
	}
}
