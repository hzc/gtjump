package Main;
import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;


public class Window extends Canvas {
	private static final long serialVersionUID = 1L;

	public Window(int width, int height, String title, GTest gt){
		JFrame frame = new JFrame(title);
		frame.setPreferredSize(new Dimension(width,height));//pref size of window
		frame.setMaximumSize(new Dimension(width,height));//max size of window
		frame.setMinimumSize(new Dimension(width,height));//min size of window
		
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//X
		frame.setResizable(false);//cannot resize
		frame.setLocationRelativeTo(null);
		frame.add(gt);//add class game
		frame.setVisible(true);
	gt.start();
	}	
}
