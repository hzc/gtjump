package Main;

import java.awt.event.KeyAdapter;  
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter  {
	private Engine engine;
	
	public KeyInput(Engine engine){
		this.engine = engine;
	}
	
		public void keyPressed(KeyEvent e){
		float key = e.getKeyCode();

		for(int i = 0; i<engine.Oblist.size(); i++){
				GameObject tempObject = engine.Oblist.get(i);//get object from linked list
				
					if(key == KeyEvent.VK_W) tempObject.setVelY(-3);//set this object/player
					if(key == KeyEvent.VK_S) tempObject.setVelY(3);
					if(key == KeyEvent.VK_A) tempObject.setVelX(-3);
					if(key == KeyEvent.VK_D)tempObject.setVelX(3);
				
		}

		if(key == KeyEvent.VK_ESCAPE)System.exit(1);
	}
	
	public void keyReleased(KeyEvent e){
		
		float key = e.getKeyCode();
		
		for(int i = 0; i<engine.Oblist.size(); i++){
			GameObject tempObject = engine.Oblist.get(i);//get object from linked list
			
	
				if(key == KeyEvent.VK_W) tempObject.setVelY(0);//set this object/player
				if(key == KeyEvent.VK_S) tempObject.setVelY(0);
				if(key == KeyEvent.VK_A) tempObject.setVelX(0);
				if(key == KeyEvent.VK_D)tempObject.setVelX(0);
				
				
			
	    }
	}
}
