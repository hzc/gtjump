package Main;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Block extends GameObject {

	Engine engine;
	Random R;
	int vy=7;
	
	public Block(float x, float y, ID id, Engine engine) {
		super(x, y, id);
		R = new Random();
		this.engine = engine;//access to all game objects
		this.y=R.nextInt(50)+240;
	}

	public void tick() {
		x-=velX+5;
		y-=vy;
		
		if(x<=-40)
			x=639;
		
		if(y<=240||y>=300)
			vy*=(-1);
	}

	public void render(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect((int)x,(int) y,40, 600);
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x,(int)y,40,600);
	}

}
