package Main;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;


public class BlockUp extends GameObject{

	Engine engine;
	Random Rando = new Random();
	
	public BlockUp(float x, float y, ID id,Engine engine) {
		super(x, y, id);
		this.engine = engine;
		
		this.y = Rando.nextInt(12)*(-10)-200;

	}

	
	public void tick() {
		x-=super.speedx;
		y+=super.speedy;
		
		if(y+400>=200||y+400<=-440)
		super.speedy*=(-1);
		
		if(x<=-25){
			x=630;
		}
		
	}

	
	public void render(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect((int)x, (int)y, 40, 400);
		
		
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x,(int)y,40,400);
	}

}
