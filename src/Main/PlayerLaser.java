package Main;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;


public class PlayerLaser extends GameObject {
	Engine engine;
	float vx,vy,MouseX,MouseY,distance;
	private BufferedImage missle;
	
	
	
		public PlayerLaser(float x, float y, ID id, Engine engine,float MouseX,float MouseY) {
			super(x, y, id);
			this.engine = engine;
			this.MouseX = MouseX;
			this.MouseY = MouseY;
			
			missle = ResourceLoader.getImage("missle.png");
		
			
		}

		@Override
		public void tick() {
			x+=vx;
			y+=vy;
			
			float diffX = x - MouseX;
			float diffY = y - MouseY;
			
		
			float distance = (float) Math.sqrt(((x-MouseX)*(x-MouseX)+(y-MouseY)*(y-MouseY)));
			vx = ((-6/distance)* diffX);
			vy= ((-6/distance)* diffY);
			
			if(x+5>=MouseX&&x+5<=MouseX+10)
					engine.Oblist.remove(this);
			
				
		}

		@Override
		public void render(Graphics g) {
			/*g.setColor(Color.BLUE);
			g.fillOval((int)x, (int)y,5, 5);*/
			
			g.drawImage(missle,(int) x, (int)y, null);

		}

		@Override
		public Rectangle getBounds() {
			
			return new Rectangle((int)x,(int)y,10,10);
		}

		
	}

