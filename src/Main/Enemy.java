package Main;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;


public class Enemy extends GameObject {
Engine engine;
float speedx,speedy;
Random R = new Random();
private BufferedImage enemy;



	public Enemy(float x, float y, ID id,Engine engine) {
		super(x, y, id);
		this.engine = engine;
		speedx=1;
		speedy=1;
		enemy = ResourceLoader.getImage("enemy.png");
		
	}

	
	public void tick() {
		x-=speedx;
		y+=speedy;
		
		if(x<=250||x>=600){
			speedx*=-1;
		}
		if(y>=300||y<=50){
			speedy*=-1;
		}

		
		collision();
	}

	
	public void collision(){
		for(int i=0;i<engine.Oblist.size();i++){
			GameObject tObject = engine.Oblist.get(i);
			if(tObject.getId()==ID.PlayerLaser){
				if(getBounds().intersects(tObject.getBounds())){

								engine.Oblist.remove(this);
								HUD.score+=1000;
								engine.addObject(new Enemy(R.nextInt(100)+500,R.nextInt(150),ID.Enemy,engine));
								engine.addObject(new Enemy(R.nextInt(100)+500,R.nextInt(150),ID.Enemy,engine));
							
								
					for(int j=0;j<engine.Oblist.size();j++){
						GameObject tObject2 = engine.Oblist.get(j);
							if(tObject2.getId()==ID.Enemy){
								engine.Oblist.remove(tObject2);
						}
					}
				}
				
				
			}
		}
		
		for(int j=0;j<engine.Oblist.size();j++){
			GameObject tObject2 = engine.Oblist.get(j);
				if(tObject2.getId()==ID.BlockDown){
					if(getBounds().intersects(tObject2.getBounds())){
							this.speedx*=-1;
							this.speedy*=-1;
						
					}
			}
		}
	
	}
		
	
	
	
	public void render(Graphics g) {
		/*g.setColor(Color.red);
		g.fillOval((int)x, (int)y, 10, 10);*/
		
		g.drawImage(enemy, (int)x, (int)y, null);
		
	}

	
	public Rectangle getBounds() {
		
		return new Rectangle((int)x,(int)y,30,30);
	}

}
