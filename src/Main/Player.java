package Main;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;


public class Player extends GameObject {
Engine engine;
float Count = 1;
Random R = new Random();
private BufferedImage ship;



	public Player(float x, float y, ID id, Engine engine) {
		super(x, y, id);
		this.engine = engine;
		ship = ResourceLoader.getImage("ship.png");
	}

	public void tick() {
		x += getVelX();
		y += getVelY();
		x = GTest.clamp(x, 0, GTest.WIDTH - 90);//cannot go out from the map
		y = GTest.clamp(y, 0, GTest.HEIGHT - 60);
		
		for(int i=0;i<engine.Oblist.size();i++)
			if(engine.Oblist.get(i).id==ID.BlockDown)
				y = GTest.clamp((int)y,0,engine.Oblist.get(i).y-64);
		
		
		collision();
	}

	
	public void render(Graphics g) {
		
		//g.setColor(Color.BLUE);
		
		//g.fillOval((int)x, (int)y, 20, 20);
		
		g.drawImage(ship, (int)x, (int)y, null);
		
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x+40,(int)y+60,50,30);
	}
	
	public void collision(){
		for(int i=0;i<engine.Oblist.size();i++){
			GameObject tObject = engine.Oblist.get(i);
			
			
			if(tObject.getId()==ID.EnemyLaser){
				if(getBounds().intersects(tObject.getBounds())){
				
						HUD.WIDTH-=10;
						engine.Oblist.remove(tObject);
						
						for(int j=0;j<engine.Oblist.size();j++){
							GameObject tObject2 = engine.Oblist.get(j);
								if(tObject2.getId()==ID.Enemy){
										engine.addObject(new Laser(engine.Oblist.get(j).x,engine.Oblist.get(j).y,ID.EnemyLaser,engine));
						}
				}
			}
			

			/*	for(int j=0;j<engine.Oblist.size();j++){
					GameObject tObject2 = engine.Oblist.get(j);
			if(tObject2.getId()==ID.BlockDown){
				if(getBounds().intersects(tObject2.getBounds())){
	
					engine.Oblist.get(0).x= tObject2.x+10;
					engine.Oblist.get(0).y= tObject2.y-20;
					}
				}}*/
		
	}
			
	
		}
	}
}
