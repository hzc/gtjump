package Main;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;


public class Laser extends GameObject {
Engine engine;
private GameObject player;
private BufferedImage badlaser;

float vx,vy;


	public Laser(float x, float y, ID id, Engine engine) {
		super(x, y, id);
		this.engine = engine;
		
		
		for(int i=0;i<engine.Oblist.size();i++)
			if(engine.Oblist.get(i).id==ID.Player)
				this.player = engine.Oblist.get(i);
		
		badlaser =  ResourceLoader.getImage("badlaser.png");
		
	}

	@Override
	public void tick() {
		x+=vx;
		y+=vy;
		
		float diffX = x - (player.getX()+50);
		float diffY = y - (player.getY()+60);
		float distance = (float) Math.sqrt((x-player.getX())*(x-player.getX())+(y-player.getY())*(y-player.getY()));
		
		vx = ((-7/distance)* diffX);
		vy= ((-7/distance)* diffY);
		
		collision();
		
	}

	
	public void collision(){
		for(int i=0;i<engine.Oblist.size();i++){
			GameObject tObject = engine.Oblist.get(i);
			
			if(tObject.getId()==ID.PlayerLaser){
				if(getBounds().intersects(tObject.getBounds())){
					engine.Oblist.remove(tObject);
					engine.Oblist.remove(this);
					
					for(int j=0;j<engine.Oblist.size();j++){
						GameObject tObject2 = engine.Oblist.get(j);
							if(tObject2.getId()==ID.Enemy){
									engine.addObject(new Laser(engine.Oblist.get(j).x,engine.Oblist.get(j).y,ID.EnemyLaser,engine));
							}
				
						}
					}
				}
		}
	}
	
	
	
	@Override
	public void render(Graphics g) {
		/*
		g.setColor(Color.RED);
		g.fillOval((int)x, (int)y,5, 5);*/
		
		g.drawImage(badlaser, (int)x,(int)y, null);
		

	}

	@Override
	public Rectangle getBounds() {
		
		return new Rectangle((int)x,(int)y,30,30);
	}

	
}
