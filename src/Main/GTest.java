package Main;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;




public class GTest extends Canvas implements Runnable {
	private static final long serialVersionUID = 1L;
	public static final float WIDTH = 640, HEIGHT = WIDTH / 12*9;
	boolean running = false;
	
	public Thread thread;
	private Engine engine;
	HUD hud;
	
	
	public GTest() {
		new Window((int)WIDTH,(int)HEIGHT,"TESTING",this);
		
		engine = new Engine();//keeps all objects and provide tick working
		this.addKeyListener(new KeyInput(engine));
		this.addMouseListener(new MouseAction(engine));
		
		
		
		

		hud = new HUD();
		engine.addObject(new Player(100,100,ID.Player,engine));
		
		for(int j=0;j<720;j+=40)
		engine.addObject(new Block(j,200,ID.BlockDown,engine));
		
	/*	for(int j=40;j<720;j+=40)
		engine.addObject(new BlockUp(WIDTH+j,0,ID.BlockUp,engine));*/
	
		engine.addObject(new Laser(580,200,ID.EnemyLaser,engine));
		engine.addObject(new Enemy(500,100,ID.Enemy,engine));
		
	}
	
	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
		
	}

	public void stop() {
		try{
			thread.join();
			running = false;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void run() {
		
		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		//int frames = 0;
		while(running){//main game loop
		long now = System.nanoTime();
		delta += (now - lastTime) /ns;
		lastTime = now;
			while(delta >= 1){
				tick();//2nd // engine tick
				delta--;		
			}
			if(running)
				render();//1st
			//	frames++;
				
				if(System.currentTimeMillis() - timer > 1000){
					timer += 1000;
				//System.out.println("FPS: "+frames);
				//	frames = 0;
				}
		  }
		stop();
	}
	
	public void tick() {
		engine.tick();
		hud.tick();
		if(hud.WIDTH==0)
		{
			System.out.println("YOUR SCORE : "+hud.score);
			stop();
		}
	}
	
	public void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if(bs==null){
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, (int)WIDTH,	(int)HEIGHT);
		engine.render(g);
		hud.render(g);
			
		g.dispose();
		bs.show();
	
	}
	
	public static float clamp(float var, float min, float max){//blocking player //cannot go out from the map
		if(var >= max)
			return var = max;
		else if(var <= min)
			return var = min;
		
		return var;
	}
	public static void main(String args[]){
		new GTest();
	}

}
